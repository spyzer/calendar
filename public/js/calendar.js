function Calendar() {
    this.today = new Date();
    this.current_month = this.today.getMonth();
    this.current_year = this.today.getFullYear();
    this.object = null;
    this.current_month_days = [];
    this.month_names_array = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];

    this.switchMonth = function(month){
        const current_date = new Date(this.current_year, this.current_month);

        current_date.setMonth(typeof month === "undefined" ? this.today.getMonth() : month);

        this.current_month = current_date.getMonth();
        this.current_year = current_date.getFullYear();

        this.getCurrentMonthDays();
        this.render();
    };

    this.nextMonth = function(){
        this.switchMonth(this.current_month + 1);
    };

    this.previousMonth = function(){
        this.switchMonth(this.current_month - 1);
    };

    this.initialize = function(DOM_object){
        this.object = DOM_object;
        this.getCurrentMonthDays();
        this.render();
    };

    this.getCurrentMonthDays = function(){
        const first_month_day = new Date(this.current_year, this.current_month, 1),
            last_month_day = new Date(this.current_year, this.current_month+1, 0),
            first_day = new Date(first_month_day.setDate(first_month_day.getDate() - (first_month_day.getDay() || 7) + 1)),
            last_day = new Date(last_month_day.setDate(last_month_day.getDate() + 7 - (last_month_day.getDay() || 7)));

        this.current_month_days = [];

        for(var current_day = first_day; current_day.getTime() <= last_day.getTime(); current_day.setDate(current_day.getDate()+1)){
            this.current_month_days.push(new Day(current_day.getTime()));
        }
    };

    this.render = function(){
        var $container = document.createElement("div");

        document.getElementById("month_name").innerHTML = this.month_names_array[this.current_month];
        document.getElementById("year").innerHTML = this.current_year;

        for(var i = 0, l = this.current_month_days.length; i < l; i++){
            const $div = document.createElement("div");

            $div.classList.add("calendar__day");

            if(this.current_month_days[i].date.getDate() === this.today.getDate()
                && this.current_month_days[i].date.getMonth() === this.today.getMonth()){
                $div.classList.add("today");
            }

            $div.innerHTML = this.current_month_days[i].date.getDate();
            $container.appendChild($div);
        }

        this.object.innerHTML = $container.outerHTML;

        Array.prototype.forEach.call(document.querySelectorAll(".calendar__day"), function(element){
            element.addEventListener("click", function(){
                const element = document.querySelector(".calendar__day.selected");

                if(element){
                    element.classList.remove("selected");
                }

                this.classList.add("selected");
            });
        });
    };
}

function Day(date) {
    this.date = new Date(date);

    this.showDate = function(){
        return this.date.getTime();
    };
}