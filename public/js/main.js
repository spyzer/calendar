document.addEventListener("DOMContentLoaded", function(){
    'use strict';

    const calendar = new Calendar();

    calendar.initialize(document.querySelector(".calendar"));

    document.getElementById("next_month").addEventListener("click", function(){
        calendar.nextMonth();
    });

    document.getElementById("previous_month").addEventListener("click", function(){
        calendar.previousMonth();
    });

    document.getElementById("default_month").addEventListener("click", function(){
        calendar.switchMonth();
    });
});